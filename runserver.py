import hashlib
import os
import shutil
import subprocess

from backend import src

ANGULAR_PROJECT_NAME = "frontend"
FLASK_PROJECT_NAME = "backend/src"

ANGULAR_PROJECT_PATH = os.path.join(os.getcwd(), ANGULAR_PROJECT_NAME)
DIST_PATH = os.path.join(ANGULAR_PROJECT_PATH, "dist", ANGULAR_PROJECT_NAME)
FLASK_STATIC_PATH = os.path.join(os.getcwd(), FLASK_PROJECT_NAME, "static")
FLASK_TEMPLATES_PATH = os.path.join(os.getcwd(), FLASK_PROJECT_NAME, "templates")


def get_file_hash(filename, blocksize=65536):
    hash_value = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash_value.update(block)
    return hash_value.hexdigest()


print("Starte Angular-Build-Prozess")
subprocess.call(("cd " + ANGULAR_PROJECT_PATH + "&& ng build --base-href /static/"), shell=True)

print("Prüfe Dateien auf Änderungen")
dist_files = os.listdir(DIST_PATH)
static_files = []
html_files = []

for file in dist_files:
    if ".js" in file or ".ico" in file:
        if file in os.listdir(FLASK_STATIC_PATH) and get_file_hash(os.path.join(DIST_PATH, file)) == \
                get_file_hash(os.path.join(FLASK_STATIC_PATH, file)):
            continue
        else:
            static_files.append(os.path.join(DIST_PATH, file) + " ")

    if ".html" in file:
        if file in os.listdir(FLASK_TEMPLATES_PATH) and get_file_hash(os.path.join(DIST_PATH, file)) == \
                get_file_hash(os.path.join(FLASK_TEMPLATES_PATH, file)):
            continue
        else:
            html_files.append(os.path.join(DIST_PATH, file) + " ")

if len(static_files) > 0:
    for file in static_files:
        shutil.copy(file, FLASK_STATIC_PATH)
    print("Es wurden {} Dateien im Ordner 'static' aktualisiert".format(len(static_files)))
if len(html_files) > 0:
    for file in html_files:
        shutil.copy(file, FLASK_TEMPLATES_PATH)
    print("Es wurden {} Dateien im Ordner 'templates' aktualisiert".format(len(html_files)))

print("Starte den Flask-Server")
src.app.run(debug=True, use_reloader=False)
