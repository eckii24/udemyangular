from flask_restful import Resource

CLASSES = ["HelloWorld"]


class HelloWorld(Resource):
    ROUTE = '/api/HelloWorld'

    def get(self):
        return {'hello': 'world'}
