import {Injectable} from '@angular/core';
import {Http, Headers, Response} from "@angular/http";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  baseUrl = "https://udemy-683fe.firebaseio.com/";
  headers = new Headers({"Content-Type": "application/json"});

  constructor(private http: Http) {
  }

  storeServers(servers: any[]) {

    return this.http.put(this.baseUrl + "data.json", servers, {headers: this.headers})
  }

  getServers() {
    return this.http.get(this.baseUrl + "data", {headers: this.headers})
      .pipe(map(
        (response: Response) => {
          const data = response.json();
          return data
        }

      ))
      .pipe(catchError(
        (error: Response) => {
            return throwError("something went wrong")
        }
      ))
  }

  getAppName(){
    return this.http.get(this.baseUrl + "appName.json").pipe(map(
      (response: Response) => {
        return response.json()
    }
    ))
  }
}
